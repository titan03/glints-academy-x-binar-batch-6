const User = require('../models/User');
const jwt = require('jsonwebtoken');

const Authentication = async (req, res, next) => {
    try {
        let token = req.headers.authorization
        let payload = await jwt.verify(token, process.env.JWT_SECRET)
        req.user = await User.findById(payload._id)
        console.log(payload._id);
        next()
    } catch (error) {
        res.status(403).json({
            status: false,
            message: [error.message]
        })
    }
}

module.exports = Authentication