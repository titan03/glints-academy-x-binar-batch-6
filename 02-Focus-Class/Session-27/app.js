require('dotenv').config();
const express = require('express')
const morgan = require('morgan')
const app = express()
require('./config/mongoose');
const router = require('./routers')

app.use(express.urlencoded({
    extended: true
}))
app.use(morgan('dev'))
app.use(express.json())
app.use(router)

module.exports = app;