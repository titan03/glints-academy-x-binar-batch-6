const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

class Users {
  static async register(req, res) {
    let {
      email,
      password
    } = req.body
    if (password !== '' || password !== null || password !== undefined) {
      password = bcrypt.hashSync(String(password), 10)
    }
    const user = await User.create({
        email,
        password
      })
      .catch((err) => {
        res.status(422).json({
          status: false,
          message: [err.message]
        })
      })

    if (user) {
      res.status(201).json({
        status: true,
        data: [user]
      })
    }
  }

  static async login(req, res) {
    const {
      email,
      password
    } = req.body
    let user = await User.findOne({
      email: email.toLowerCase()
    }).catch((err) => {
      res.status(422).json({
        status: false,
        message: [err.message]
      })
    })

    if (user && bcrypt.compareSync(password, user.password)) {
      let token = jwt.sign({
        _id: user._id,
        email: user.email
      }, process.env.JWT_SECRET)
      res.status(202).json({
        status: true,
        Authorization: token
      })
    }
  }

}

module.exports = Users