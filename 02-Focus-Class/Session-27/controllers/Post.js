const Post = require('../models/Post');

class Posts {
    static async createPost(req, res) {
        const {
            title,
            body,
            approved,
        } = req.body

        let post = await Post.create({
            title,
            body,
            approved
        }).catch((err) => {
            res.status(422).json({
                status: false,
                message: [err.message]
            })
        })

        if (post) {
            res.status(201).json({
                status: true,
                data: [post]
            })
        }
    }

    static async getPosts(req, res) {
        const posts = await Post.find()
            .catch((err) => {
                res.status(422).json({
                    status: false,
                    message: [err.message]
                })
            })

        if (posts) {
            res.status(200).json({
                status: true,
                data: [posts]
            })
        }
    }

    static async getOnePost(req, res) {
        const post = await Post.findOne({
            _id: req.params._id
        }).catch((err) => {
            res.status(422).json({
                status: false,
                message: [err.message]
            })
        })

        if (post) {
            res.status(200).json({
                status: true,
                data: [post]
            })
        }
    }

    static async editPost(req, res) {
        const id = req.params._id
        const {
            title,
            body,
            approved
        } = req.body

        const post = await Post.findByIdAndUpdate({
            _id: id
        }, {
            title,
            body,
            approved
        }).catch((err) => {
            res.status(422).json({
                status: false,
                message: [err.message]
            })
        })

        if (post) {
            res.status(202).json({
                status: true,
                message: `Post with id: ${id} has been updated`
            })
        }
    }

    static async deletePost(req, res) {
        const id = req.params._id
        const post = await Post.findByIdAndRemove({
                _id: id
            })
            .catch((err) => {
                res.status(422).json({
                    status: false,
                    message: [err.message]
                })
            })

        if (post) {
            res.status(200).json({
                status: true,
                message: `Post with id: ${id} has been deleted`
            })
        }
    }
}

module.exports = Posts