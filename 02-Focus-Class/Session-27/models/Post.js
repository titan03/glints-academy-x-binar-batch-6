const mongoose = require('mongoose');
let schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    approved: {
        type: Boolean,
        required: true,
        default: false
    }
});
let Post = mongoose.model('Post', schema);
module.exports = Post