const router = require('express').Router();
const Post = require('../controllers/Post');
const Auth = require('../middlewares/Auth');

router.post('/', Auth, Post.createPost)
router.get('/', Auth, Post.getPosts)
router.get('/:_id', Auth, Post.getOnePost)
router.put('/:_id', Auth, Post.editPost)
router.delete('/:_id', Auth, Post.deletePost)

module.exports = router