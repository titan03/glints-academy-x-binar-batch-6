const router = require('express').Router();
const Post = require('./Post');
const User = require('./User');

router.use('/api/v1/posts', Post)
router.use('/api/v1/users', User)

module.exports = router