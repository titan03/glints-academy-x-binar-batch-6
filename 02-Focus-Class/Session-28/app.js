require('dotenv').config();
const express = require('express');
const app = express()
const PORT = process.env.PORT || 6000
const logger = require('morgan');
const router = require('./router');

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

app.use('/api/v1', router)





app.listen(PORT, () => {
    console.log(`Server running on ${Date(Date.now())}`);
    console.log(`Server listening on PORT: ${PORT}`);
})