const router = require('express').Router();
const Mail = require('./controller');

router.get('/mail', Mail.sendMail)

module.exports = router