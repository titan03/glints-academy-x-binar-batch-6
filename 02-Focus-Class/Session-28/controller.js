const sgMail = require('@sendgrid/mail');

class Mailer {
    static async sendMail(req, res) {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY);
        const msg = {
            to: 'titanioy98@gmail.com',
            from: 'titanioyudista98@gmail.com',
            subject: 'Sending with Twilio SendGrid is Fun',
            text: 'and easy to do anywhere, even with Node.js',
            // html: '<strong>and easy to do anywhere, even with Node.js</strong>',
        };

        try {
            const result = await sgMail.send(msg);
            res.status(200).json({
                status: 'Success',
                result
            })
        } catch (error) {
            console.error(error);
            res.status(422).json({
                status: 'Failed',
                Error: [error.message]
            })
            if (error.response) {
                console.error(error.response.body)
            }
        }
    }
}

module.exports = Mailer