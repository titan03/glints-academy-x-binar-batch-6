const {
    Post
} = require('../models');

class Posts {
    static async create(req, res) {
        const {
            title,
            body
        } = req.body
        Post.create({
                title,
                body
            })
            .then((result) => {
                res.status(201).json({
                    status: 'Success',
                    data: [result]
                })
            }).catch((err) => {
                res.status(422).json({
                    status: 'Failed',
                    Error: [err.message]
                })
            });
    }

    static async get(req, res) {
        Post.findAll()
            .then((result) => {
                res.status(200).json({
                    status: 'Success',
                    data: [result]
                })
            }).catch((err) => {
                res.status(422).json({
                    status: 'Failed',
                    Error: [err.message]
                })
            });
    }

    static async edit(req, res) {
        const id = req.params.id
        const {
            title,
            body
        } = req.body
        Post.update({
                title,
                body
            }, {
                where: {
                    id
                }
            })
            .then(() => {
                res.status(200).json({
                    status: 'Success',
                    message: `Post with id ${id} has been updated`
                })
            }).catch((err) => {
                res.status(422).json({
                    status: 'Failed',
                    Error: [err.message]
                })
            });
    }

    static async delete(req, res) {
        const id = req.params.id
        Post.destroy({
            where: {
                id
            }
        }).then(() => {
            res.status(200).json({
                status: 'Success',
                message: `Post with id ${id} has been deleted`
            })
        }).catch((err) => {
            res.status(422).json({
                status: 'Failed',
                Error: [err.message]
            })
        });
    }
}

module.exports = Posts