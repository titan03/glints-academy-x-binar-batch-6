const router = require('express').Router();
const Posts = require('./Post');
const Index = require('./indexController');

router.use('/admin', Posts)
router.use('/index', Index)

module.exports = router