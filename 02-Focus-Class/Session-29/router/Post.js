const router = require('express').Router();
const Posts = require('../controllers/Posts');

router.post('/', Posts.create)
router.get('/', Posts.get)
router.put('/:id', Posts.edit)
router.delete('/:id', Posts.delete)

module.exports = router