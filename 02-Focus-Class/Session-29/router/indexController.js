const router = require('express').Router();
const Index = require('../controllers/indexController');

router.get('/', Index.home)

module.exports = router