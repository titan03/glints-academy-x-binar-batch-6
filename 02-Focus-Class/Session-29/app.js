const express = require('express');
const app = express()
const PORT = 8000
const Router = require('./router');

app.set('view engine', 'pug')
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))
app.use(express.static('public'))

app.use(Router)

app.listen(PORT, () => {
    console.log(`Server running on ${Date(Date.now())}`);
    console.log(`Server listening on port: ${PORT}`);
})