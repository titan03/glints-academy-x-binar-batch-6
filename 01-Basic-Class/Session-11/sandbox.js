Array.prototype.sample = function () {
    return this[
        Math.floor(
            Math.random() * this.length
        )
    ]
}

class Human {
    constructor(props) {
        let { name, address, lang } = props
        this.name = name
        this.address = address
        this.lang = lang
    }

    introduce() {
        console.log(`Hi, my name is ${this.name}`);
    }

    cook() {
        console.log(`i can cooking pizza, sushi, gudeg`);
    }
}

class Chef extends Human {
    constructor(props) {
        super(props)
        this.causines = props.causines
    }

    promote() {
        console.log(`Can cook some cousines ${this.causines.sample()}`);
    }

    cook() {
        // ! Super just like this, but super specifically used for called parents
        super.introduce()
        this.promote()
        super.cook()
    }
}

const Titan = new Chef({
    name: 'Titan',
    address: 'Jaksel',
    lang: 'Bahasa',
    causines: ['javanisch', 'italienish', 'japanisch']
})

Titan.cook()