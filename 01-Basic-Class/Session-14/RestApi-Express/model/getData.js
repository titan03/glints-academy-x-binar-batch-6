const fs = require('fs');

class Model {
    static getData(file, cb) {
        fs.readFile(file, (err, data) => {
            if (err) {
                cb(err, null)
            } else {
                cb(null, JSON.parse(data))
            }
        })
    }
}

module.exports = Model
