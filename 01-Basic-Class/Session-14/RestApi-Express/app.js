const express = require('express');
const app = express()
const PORT = process.env.PORT || 3000

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const Router = require('./router/index');

app.use(Router)
// app.get('/', (req, res) => {
//     res.send('<h1>Hello World</h1>')
// })

app.listen(PORT, () => {
    console.log(`Server running on PORT: ${PORT}`);

})