const Model = require('../model/getData');
const user = '/home/titan/glints-academy-x-binar-batch-6/01-Basic-Class/Session-14/RestApi-Express/DB/user.json'
const dataUser = require('../DB/user.json');

class Users {
    static getUsers(req, res) {
        Model.getData(user, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {
                data = { ...dataUser }
                delete (data.password)
                res.status(200).json({
                    data
                })
            }
        })
    }

    static login(req, res) {
        Model.getData(user, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {
                if (dataUser.email !== req.body.email)
                    return res.status(401).json({
                        status: false,
                        message: "Email doesn't exist!"
                    })

                if (dataUser.password !== req.body.password)
                    return res.status(401).json({
                        status: false,
                        message: "Wrong password!"
                    })

                res.status(200).json({
                    result: data,
                    status: true,
                    message: "Succesfully logged in"
                })
            }
        })
    }
}

module.exports = Users
