const Model = require('../model/getData');
const product = '/home/titan/glints-academy-x-binar-batch-6/01-Basic-Class/Session-14/RestApi-Express/DB/product.json'
const dataProduct = require('../DB/product.json');

class Products {
    static getAll(req, res) {
        Model.getData(product, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {
                res.status(200).json({
                    data
                })
            }
        })
    }

    static getAvailable(req, res) {
        Model.getData(product, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {
                data = dataProduct.filter(i => i.stock > 0)
                res.status(200).json({
                    data
                })
            }
        })
    }
}

module.exports = Products
