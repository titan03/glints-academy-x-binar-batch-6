const router = require('express').Router();
const User = require('../controller/user');

router.get('/show', User.getUsers)
router.post('/login', User.login)

module.exports = router
