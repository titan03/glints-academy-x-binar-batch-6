const router = require('express').Router();
const Product = require('../controller/product');

router.get('/show', Product.getAll)
router.get('/available', Product.getAvailable)

module.exports = router
