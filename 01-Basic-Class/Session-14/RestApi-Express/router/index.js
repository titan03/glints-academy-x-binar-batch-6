const router = require('express').Router();
const Product = require('./product');
const User = require('./user');

router.use('/products', Product)
router.use('/users', User)

module.exports = router
