const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');
// console.log(data);

/*
 * Code Here!
 * */

// Optional
function clean(data) {
    return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
    // console.log('data :>> ', data);
    data = clean(data)

    for (let i = 0; i < data.length; i++) {
        for (let j = i + 1; j < data.length; j++) {
            if (data[i] > data[j]) {
                let temp = data[i]
                data[i] = data[j]
                data[j] = temp
            }
        }
    }

    return data
}

// Should return array
function sortDecending(data) {

    data = clean(data)

    for (let i = 0; i < data.length; i++) {
        for (let j = i + 1; j < data.length; j++) {
            if (data[i] < data[j]) {
                let temp = data[i]
                data[i] = data[j]
                data[j] = temp
            }
        }
    }
    // console.log(data);
    return data
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);