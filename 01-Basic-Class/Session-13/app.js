/*
  HTTP module. Node.js udah punya built-in HTTP Module.

  Ruby => Puma
  PHP => Apache

  Node.JS => http

  */

const http = require('http');
const PORT = 4000;
const Products = require('./model/product');
const app = http.createServer((req, res) => {
    switch (req.url) {
        case '/':
            res.write("Hello World\n");
            break;

        case '/products':
            res.write(JSON.stringify(Products.all))
            break;

        case '/error':
            try {
                throw new Error("This is error!");
            }
            catch (err) {
                res.writeHead(500);
                res.write(err.message);
            }
            break;

        default:
            res.writeHead(404);
            res.write("404 Not Found!\n")
    }

    res.end();
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
