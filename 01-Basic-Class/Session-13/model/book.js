const Record = require('./record');

class Book extends Record {
    static properties = {
        title: {
            type: 'string',
            required: true
        },
        author: {
            type: 'string',
            required: true
        },
        price: {
            type: 'number',
            required: true
        },
        publisher: {
            type: 'string',
            required: true
        },
    }
}

module.exports = Book;