const fs = require('fs');
const path = require('path');
const readline = require('./readline');
const db = require('./user.json');

function register(email, password) {
    const dbPath = path.resolve(__dirname, '.', 'user.json')
    const user = {
        email: email.toLowerCase(),
        password
    }

    db.push(user)
    fs.writeFileSync(dbPath, JSON.stringify(db, null, 4))
    readline.close()
}

function login(email, password) {
    for (let i = 0; i < db.length; i++) {
        if (db[i].email === email && db[i].password === password) {
            console.log('Login success');
        } else {
            console.log('Login error, please login again');
        }
    }
}

module.exports = {
    register,
    login
};