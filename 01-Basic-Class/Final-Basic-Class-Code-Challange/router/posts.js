const router = require('express').Router();
const Posts = require('../controller/posts');
const Auth = require('../middlewares/Auth');
const CheckOwners = require('../middlewares/checkOwners');
const Admin = require('../middlewares/admin');

//! Router Admin
router.get('/admin', Admin, Posts.findAll)
router.get('/:id', Admin, Posts.findOne)
router.put('/admin/:id', Admin, Posts.updateAdmin)
router.delete('/admin/:id', Admin, Posts.delete)

// ! Router Member
router.get('/', Auth, Posts.findAllTrue)
router.post('/', Auth, Posts.create)
router.put('/:id', Auth, CheckOwners('Post'), Posts.update)
router.delete('/:id', Auth, CheckOwners('Post'), Posts.delete)



module.exports = router