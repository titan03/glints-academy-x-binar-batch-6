const router = require('express').Router();
const Posts = require('./posts');
const Users = require('./user');

router.use('/api/v1/users', Users)
router.use('/api/v1/posts', Posts)

module.exports = router