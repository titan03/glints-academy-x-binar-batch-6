const { User } = require('../models');
const SuccessHandler = require('../helper/response');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

class Users {
    static register(req, res, next) {
        const { email, password, role } = req.body
        User.create({
            email,
            password,
            role
        })
            .then((user) => {
                SuccessHandler(res, 201, user, 'successfully created member')
            }).catch((err) => {
                next(err)
            });
    }

    static login(req, res, next) {
        const { email, password } = req.body
        User.findOne({ where: { email: email.toLowerCase() } })
            .then((user) => {
                // console.log(user.role, '==============ini data user ======================');
                if (user.role === 'member' && bcrypt.compareSync(password, user.password)) {
                    let access_token = jwt.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET)
                    SuccessHandler(res, 202, access_token, 'Successfully Login')

                } else if (user.role === 'admin' && bcrypt.compareSync(password, user.password)) {
                    let access_token = jwt.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET_ADMIN)
                    SuccessHandler(res, 202, access_token, 'Successfully Login')
                }
            }).catch((err) => {
                next(err)
            });
    }
}

module.exports = Users
