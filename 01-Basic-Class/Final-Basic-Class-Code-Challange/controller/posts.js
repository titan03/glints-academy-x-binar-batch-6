const { Post, User } = require('../models');
const SuccessHandler = require('../helper/response');

class Posts {
    static findAll(req, res, next) {
        Post.findAll({ include: User })
            .then((result) => {
                SuccessHandler(res, 200, result, 'successfully getAll posts')
            }).catch((err) => {
                next(err)
            });
    }

    static findAllTrue(req, res, next) {
        Post.findAll({ where: { approved: true } })
            .then((result) => {
                SuccessHandler(res, 200, result, 'successfully getAll posts with approved true')
            }).catch((err) => {
                next(err)
            });
    }

    static findOne(req, res, next) {
        Post.findOne({ where: { id: req.params.id } })
            .then((result) => {
                if (result) {
                    SuccessHandler(res, 200, result, 'successfully get one posts')
                } else {
                    res.status(404).json({
                        status: 'Fail',
                        message: 'Not Found'
                    })
                }
            }).catch((err) => {
                next(err)
            });
    }

    static create(req, res, next) {
        const { title, body, approved } = req.body
        Post.create({ title, body, approved, UserId: req.user.id })
            .then((result) => {
                SuccessHandler(res, 201, result, 'successfully cretae Post')
            }).catch((err) => {
                next(err)
            });
    }

    static update(req, res, next) {
        const { title, body, approved } = req.body
        Post.update({ title, body, approved }, { where: { id: req.params.id } })
            .then((result) => {
                SuccessHandler(res, 200, result, 'successfully update post')
            }).catch((err) => {
                next(err)
            });
    }

    static delete(req, res, next) {
        let deleteId = req.params.id
        Post.destroy({ where: { id: deleteId } })
            .then((result) => {
                SuccessHandler(res, 200, result, 'successfully deleted post')
            }).catch((err) => {
                next(err)
            });
    }

    static updateAdmin(req, res, next) {
        let updateId = req.params.id
        const { approved } = req.body
        Post.update({ approved }, { where: { id: updateId } })
            .then((result) => {
                SuccessHandler(res, 200, result, 'successfully updated Post by Admin')
            }).catch((err) => {
                next(err)
            });
    }
}

module.exports = Posts
