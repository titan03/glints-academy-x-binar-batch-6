const express = require('express');
const app = express()
const logger = require('morgan')
const Router = require('./router');
const ErrorHandler = require('./middlewares/exceptionError');
require('dotenv').config()


app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(logger('dev'))

app.use(Router)

//! Error Handler
app.use(ErrorHandler)

app.listen(process.env.PORT, () => {
    console.log(`Server started on ${Date(Date.now())}`);
    console.log(`Server is listening on port ${process.env.PORT}`);
});