const serveRes = (res, code, result, msg) => {
    if (code === 200) {
        return res.status(code).json({
            status: true,
            msg,
            result
        })
    } else if (code === 201) {
        return res.status(code).json({
            status: true,
            msg,
            result
        })
    } else if (code === 202) {
        return res.status(code).json({
            status: true,
            msg,
            token: result
        })
    }
}

module.exports = serveRes