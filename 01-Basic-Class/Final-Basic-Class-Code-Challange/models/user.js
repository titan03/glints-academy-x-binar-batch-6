'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.Sequelize.Model

  class User extends Model { }

  User.init({
    email: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input your's email`
        },
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input your's password and must be secure`
        }
      }
    },
    role: {
      type: DataTypes.STRING,
      defaultValue: 'member',
      validate: {
        isIn: [['admin', 'member']]
      }
    }
  }, {
    hooks: {
      beforeValidate(instance) {
        instance.email = instance.email.toLowerCase()
      },
      beforeCreate(instance) {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(instance.password, salt);
        instance.password = hash
      }
    }, sequelize
  })

  // const User = sequelize.define('User', {

  // }, {});
  User.associate = function (models) {
    // associations can be defined here
    User.hasMany(models.Post, { foreignKey: 'UserId' })
  };
  return User;
};