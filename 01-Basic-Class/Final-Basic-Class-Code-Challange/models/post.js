'use strict';
module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.Sequelize.Model

  class Post extends Model { }

  Post.init({
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'Please input title'
        }
      }
    },
    body: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: {
          msg: 'Please input body'
        }
      }
    },
    approved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      validate: {
        notEmpty: {
          msg: 'Please input boolean'
        },
        isIn: [[false, true]]
      }
    },
    UserId: DataTypes.INTEGER
  }, { sequelize })

  // const Post = sequelize.define('Post', {

  // }, {});
  Post.associate = function (models) {
    // associations can be defined here
    Post.belongsTo(models.User, {
      foreignKey: 'UserId'
    })
  };
  return Post;
};