const jwt = require('jsonwebtoken');
const { User } = require('../models');

const auth_for_admin = async (req, res, next) => {
    try {
        let token = req.headers.authorization
        let decode = await jwt.verify(token, process.env.JWT_SECRET_ADMIN)
        let foundAdmin = await User.findByPk(decode.id)
        console.log(foundAdmin);

        if (foundAdmin.role === 'admin') {
            res.admin = true
        }
        next()
    } catch (err) {
        throw new Error('invalid token')
    }
}

module.exports = auth_for_admin