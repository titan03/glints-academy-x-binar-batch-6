const models = require('../models');

module.exports = (modelName) => { //! parameter modelName
    const Model = models[modelName]

    return async (req, res, next) => {
        let instance = await Model.findByPk(req.params.id)
        if (instance.UserId !== req.user.id) {
            res.status(403)
            next(new Error("Forbidden"))
        } else {
            next()
        }
    }
}