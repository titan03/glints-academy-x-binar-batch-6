'use strict';
const bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      email: 'admin0@mail.com',
      password: bcrypt.hashSync('123456', bcrypt.genSaltSync(10)),
      role: 'admin',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      email: 'admin1@mail.com',
      password: bcrypt.hashSync('123456', bcrypt.genSaltSync(10)),
      role: 'admin',
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  }
};
