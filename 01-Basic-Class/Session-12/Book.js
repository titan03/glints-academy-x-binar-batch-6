const Record = require('./Record.js');

class Book extends Record {
    static properties = [
        "title",
        "author",
        "price",
        "publisher"
    ]

    save() {
        this.all.forEach(i => {
            if (i.title === this.title) {
                throw new Error('It already exist');
            }
        })
        super.save();
    }
}

let data = new Book({
    title: 'Jejak Si Budi',
    author: 'Pak Budi',
    price: 3000000,
    publisher: 'Trans 7'
})

data.save()