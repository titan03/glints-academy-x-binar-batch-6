const fs = require('fs');

class Record {
    constructor(props) {
        if (this.constructor === Record)
            throw new Error("Can't instantiate from Record");

        this._validate(props);
        this._set(props);
    }

    _validate(props) {
        if (typeof props !== 'object' || Array.isArray(props))
            throw new Error("Props must be an object");


        this.constructor.properties.forEach(i => {
            if (!Object.keys(props).includes(i))
                throw new Error(`${this.constructor.name}: ${i} is required`)
        })
    }

    _set(props) {
        this.constructor.properties.forEach(i => {
            this[i] = props[i];
        })
    }

    get all() {
        try {
            return require(`${__dirname}/${this.constructor.name}.json`)
        }
        catch {
            return []
        }
    }

    static read() {
        const read = fs.readFileSync('./home/titan/glints-academy-x-binar-batch-6/01-Basic-Class/Session-12/Book.json', 'utf8')
        const parsed = JSON.parse(read)
        console.log(parsed);
        // return parsed
    }

    static find(id) {

    }

    static update(id) {

    }

    static delete(id) {

    }

    save() {
        fs.writeFileSync(
            `${__dirname}/${this.constructor.name}.json`,
            JSON.stringify([...this.all, { id: this.all.length + 1, ...this }], null, 2)
        );
    }
}

// Record.read()
module.exports = Record 
