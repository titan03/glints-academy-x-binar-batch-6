'use strict'

const Model = require('../model/Model');
const View = require('../view/View');

class Controller {
    static help() {
        View.help()
    }

    static error() {
        View.error()
    }

    static show() {
        let data = Model.show()
        View.show(data)
    }

    static add(data) {
        let product = Model.add(data[0], Number(data[1]), Number(data[2]))
        View.message(console.log(`Successfully added new data item ${product.name}`))
    }

    static delete(id) {
        let data = Model.delete(id)
        View.message(console.log(`Successfully deleted data item ${data.name}`))
    }

    static findById(id) {
        let data = Model.findById(id)
        View.findById(data)
    }

    static update(id) {
        let data = Model.update(id)
        View.message(console.log(`Successfully updated data id ${data.id}`))
    }

}

module.exports = Controller
