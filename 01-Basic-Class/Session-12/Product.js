const Record = require('./Record');

class Product extends Record {
    static properties = [
        "name",
        "price",
        "stock"
    ]

    // constructor(props) {
    //     super(props)
    // }

    save() {
        this.all.forEach(i => {
            if (i.name === this.name) {
                throw new Error('It already exist');
            }
        })
        super.save();
    }
}

let newProduct = new Product({
    name: 'Jeans',
    price: 300000,
    stock: 8
})

newProduct.save()