const router = require('express').Router();
const Product = require('./product');
const User = require('./User');

router.use('/product', Product)
router.use('/user', User)

module.exports = router
