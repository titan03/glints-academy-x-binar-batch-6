const { Product } = require('../models');
const { Op } = require('sequelize');

class Products {
    static getAll(req, res) {
        Product.findAll()
            .then((result) => {
                if (result) {
                    res.status(200).json({
                        status: 'success',
                        result
                    })
                } else {
                    res.status(404).json({
                        status: 'not success find data',
                        message: `Data products not found`
                    })
                }
            }).catch((err) => {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            });
    }

    static getOne(req, res) {
        Product.findByPk(req.params.id)
            .then((result) => {
                if (result) {
                    res.status(200).json({
                        status: 'success',
                        result
                    })
                } else {
                    res.status(404).json({
                        status: 'not success find data',
                        message: `Data product not found`
                    })
                }
            }).catch((err) => {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            });
    }

    static getAvailable(req, res) {
        Product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0
                }
            }
        })
            .then((result) => {
                res.status(200).json({
                    status: 'success',
                    result
                })
            }).catch((err) => {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            });
    }

    static createProduct(req, res) {
        const { name, price, stock } = req.body
        Product.create({
            name,
            price,
            stock
        })
            .then((result) => {
                res.status(201).json({
                    result,
                    status: `Successfully added new product`
                })
            }).catch((err) => {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            });
    }

    static updateProduct(req, res) {
        let productId = req.params.id
        Product.update(req.body, { where: { id: productId } })
            .then(() => {
                res.status(200).json({
                    status: 'Successfully updated data product',
                    message: `Product id: ${productId} has been updated`
                })
            }).catch((err) => {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            });
    }

    static deleteProduct(req, res) {
        let deleteId = req.params.id

        Product.destroy({ where: { id: deleteId } })
            .then((result) => {
                if (result) {
                    res.status(200).json({
                        status: 'success',
                        message: `Product id: ${deleteId} has been deleted`
                    })
                } else {
                    res.status(404).json({
                        message: `Data product not found`
                    })
                }
            }).catch((err) => {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            });
    }
}

module.exports = Products
