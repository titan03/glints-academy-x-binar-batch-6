'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Products', [{
      name: 'Sikat Gigi',
      price: 15000,
      stock: 9,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Pasta Gigi',
      price: 20000,
      stock: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Handuk',
      price: 150000,
      stock: 6,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
