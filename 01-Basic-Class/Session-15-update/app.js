const express = require('express');
const app = express()
const PORT = 3000
const Router = require('./router');

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(Router)

app.get('/', (req, res) => {
    res.send('<h1> Welcome To My Mini App </h1>')
})

app.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`);
})