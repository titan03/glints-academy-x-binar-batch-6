const express = require('express');
const app = express()
const PORT = process.env.PORT || 3010

const Router = require('./router/index');
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(Router)

app.get('/', (req, res) => {
    res.send(`<h1> Welcome to mini app </h1>`)
})

app.listen(PORT, () => {
    console.log(`Server runnign on PORT: ${PORT}`);
})