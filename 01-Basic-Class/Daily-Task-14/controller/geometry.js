const Model = require('../model/model');
const Data_area = require('../db/area.json');
const Data_volume = require('../db/volume.json');
const Area = '/home/titan/glints-academy-x-binar-batch-6/01-Basic-Class/Daily-Task-14/db/area.json'
const Volume = '/home/titan/glints-academy-x-binar-batch-6/01-Basic-Class/Daily-Task-14/db/volume.json'

class Controller {
    static circleArea(req, res) {
        Model.getData(Area, (err, data) => {
            // console.log(Data_area);

            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {
                console.log(Data_area.name);


                if (Data_area.name !== req.body.name) {
                    return res.status(401).json({
                        status: false,
                        message: "input name wrong!"
                    })
                }

                if (Data_area.phi !== req.body.phi && typeof Data_area.phi !== 'number') {
                    return res.status(401).json({
                        status: false,
                        message: "input must number & number must 3.14"
                    })
                }

                if (Data_area.radius !== req.body.radius && typeof Data_area.radius !== 'number') {
                    return res.status(401).json({
                        status: false,
                        message: "input must number & number must 3"
                    })
                }

                if (Data_area.state_number !== req.body.state_number && typeof Data_area.state_number !== 'number') {
                    return res.status(401).json({
                        status: false,
                        message: "input must number & number must 2"
                    })
                }

                res.status(200).json({
                    result: data,
                    status: true,
                    message: 'success'
                })
            }
        })
    }

    static squareArea(req, res) {
        Model.getData(Area, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {

            }
        })
    }

    static triangleArea(req, res) {
        Model.getData(Area, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {

            }
        })
    }

    static cubeVolume(req, res) {
        Model.getData(Volume, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {

            }
        })
    }

    static tubeVolume(req, res) {
        Model.getData(Volume, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {

            }
        })
    }

}

module.exports = Controller


/**
    POST /calculate/circleArea
    POST /calculate/squareArea
    POST /calculate/cubeVolume
    POST /calculate/tubeVolume
    POST /calculate/triangleArea
 */