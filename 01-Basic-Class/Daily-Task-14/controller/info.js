const Model = require('../model/model');
const info = '/home/titan/glints-academy-x-binar-batch-6/01-Basic-Class/Daily-Task-14/db/info.json'
class Infos {
    static infotmation(req, res) {
        Model.readData(info, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            } else {
                res.status(200).json({
                    data
                })
            }
        })
    }
}

module.exports = Infos
