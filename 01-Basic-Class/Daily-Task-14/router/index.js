const router = require('express').Router();
const Info = require('./info');
const Geo = require('./geometry');

router.use('/infos', Info)
router.use('/calculate', Geo)


module.exports = router
