const router = require('express').Router();
const Geo = require('../controller/geometry');

router.post('/circleArea', Geo.circleArea)

module.exports = router