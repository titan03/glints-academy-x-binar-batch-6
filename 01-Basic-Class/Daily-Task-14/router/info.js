const router = require('express').Router();
const Info = require('../controller/info');

router.get('/geometry', Info.infotmation)

module.exports = router
