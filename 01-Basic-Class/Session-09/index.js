const person = {
    name: `Bambang`,
    address: `Jaksel`
}

person.gender = 'Male'
person.isMarried = true


person[person.gender === 'Male' ? 'Wife' : 'Husband'] = {
    name: 'Someone',
    gender: person.gender === 'Male' ? 'Female' : 'Male'
}
console.log(person);