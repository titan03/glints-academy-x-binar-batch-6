const express = require('express');
const app = express()
const Router = require('./router');
const ExceptionError = require('./middlewares/exceptionHandler');
const logger = require('morgan')
require('dotenv').config()
// console.log(ExceptionError);


app.use(logger('dev')) //! can use 'dev', 'tiny'
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get('/', (req, res) => {
    res.send('<h1> Welcome To My Mini App </h1>')
})

//! Router User and Product
app.use(Router)

//! Error Handler
// ExceptionError.forEach(handler => app.use(handler))
app.use(ExceptionError)


//! Linsten on PORT
app.listen(process.env.PORT, () => {
    console.log(`Server started on ${Date(Date.now())}`);
    console.log(`Server is listening on port ${process.env.PORT}`);
});