const { Product } = require('../models');
const { Op } = require('sequelize');
const createError = require('http-errors')
const successHandler = require('../helper/successHandler');

class Products {
    static getAll(req, res, next) {
        Product.findAll()
            .then((result) => {
                successHandler(res, 200, result, 'successfully get all product')
            }).catch((err) => {
                next(err)
            });
    }

    static getOne(req, res, next) {
        Product.findByPk(req.params.id)
            .then((result) => {
                if (result) {
                    successHandler(res, 200, result, 'successfully get one product')
                } else {
                    return next(createError(404, 'product not found'))
                }
            }).catch((err) => {
                next(err)
            });
    }

    static getAvailable(req, res, next) {
        Product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0
                }
            }
        })
            .then((result) => {
                successHandler(res, 200, result, 'successfully get product available')
            }).catch((err) => {
                next(err)
            });
    }

    static createProduct(req, res, next) {
        const { name, price, stock } = req.body
        Product.create({
            name,
            price,
            stock
        })
            .then((result) => {
                successHandler(res, 201, result, 'successfully created new product')
            }).catch((err) => {
                next(err)
            });
    }

    static updateProduct(req, res, next) {
        let productId = req.params.id
        Product.update(req.body, { where: { id: productId } })
            .then((result) => {
                successHandler(res, 201, result, `Succeccfully updated product id: ${productId}`)
            }).catch((err) => {
                next(err)
            });
    }

    static deleteProduct(req, res, next) {
        let deleteId = req.params.id

        Product.destroy({ where: { id: deleteId } })
            .then((result) => {
                if (result) {
                    successHandler(res, 200, result, `Succeccfully deleted product id: ${deleteId}`)
                } else {
                    return next(createError(404, 'data not found'))
                }
            }).catch((err) => {
                next(err)
            });
    }
}

module.exports = Products