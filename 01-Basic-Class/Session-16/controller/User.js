const { User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const successHandler = require('../helper/successHandler');

class Users {
    static register(req, res, next) {
        User.create({
            email: req.body.email,
            password: req.body.password
        })
            .then((result) => {
                successHandler(res, 201, result, 'user has been created')
            }).catch((err) => {
                next(err)
            });
    }

    static login(req, res, next) {
        User.findOne({
            where: { email: req.body.email.toLowerCase() }
        })
            .then((user) => {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    let access_token = jwt.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET)
                    successHandler(res, 202, access_token, 'Successfully Login')
                } else {
                    return next(createError(401, 'input email or password wrongs'))
                }
            }).catch((err) => {
                next(err)
            });
    }


    // static auth(req, res) {
    //     let token = req.headers.authorization;
    //     let payload = jwt.verify(token, process.env.JWT_SECRET)

    //     console.log(payload)
    //     // Find user!

    //     User.findOne({
    //         where: { email: req.body.email }
    //     })
    //         .then((payload) => {
    //             console.log(payload);

    //             if (!result.id) {
    //                 res.status(404).json({
    //                     message: `User cannot found`
    //                 })
    //             }
    //             if (result.id === req.params.id) {
    //                 res.status(201).json({
    //                     status: `success`,
    //                     result
    //                 })
    //             }
    //             else {
    //                 throw createError(401, "You are not authorized")
    //             }
    //         }).catch((err) => {
    //             res.status(422).json({
    //                 message: `Interal server error : ${err}`
    //             })
    //         });

    //     // res.end()

    // }
}

module.exports = Users
