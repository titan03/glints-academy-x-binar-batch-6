'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sequelize = sequelize.Sequelize
  const Model = Sequelize.Model

  class Product extends Model { }

  Product.init({
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input name product`
        }
      }
    },
    price: {
      type: DataTypes.FLOAT,
      validate: {
        notEmpty: {
          msg: `Please input price product`
        },
        isFloat: true
      }
    },
    stock: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          msg: `Please input stock product`
        },
        isInt: true
      }
    }
  }, { sequelize })

  // const Product = sequelize.define('Product', {

  // }, {});
  Product.associate = function (models) {
    // associations can be defined here
  };
  return Product;
};