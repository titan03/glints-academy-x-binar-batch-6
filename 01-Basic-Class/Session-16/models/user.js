'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const Sequelize = sequelize.Sequelize
  const Model = Sequelize.Model
  class User extends Model { }

  User.init({
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    password: DataTypes.STRING
  }, {
    hooks: {
      beforeValidate(instance) {
        instance.email = instance.email.toLowerCase()
      },
      beforeCreate(instance) {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(instance.password, salt);
        instance.password = hash
      }
    },
    sequelize
  })
  // const User = sequelize.define('User', {
  // }, {});
  User.associate = function (models) {
    // associations can be defined here
  };
  return User;
};