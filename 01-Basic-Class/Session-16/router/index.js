const router = require('express').Router();
const Product = require('./product');
const User = require('./User');

router.use('/api/v1/products', Product)
router.use('/api/v1/users', User)

module.exports = router
