const router = require('express').Router();
const Product = require('../controller/Product');
const Authentication = require('../middlewares/Auth');

//! GET data Products
router.get('/', Product.getAll)
router.get('/available', Product.getAvailable)
router.get('/:id', Product.getOne)

//!CREATE data PRODUCT
router.post('/', Authentication, Product.createProduct)

//!Update data PRODUCT
router.put('/:id', Authentication, Product.updateProduct)

//!Delete data PRODUCT
router.delete('/:id', Authentication, Product.deleteProduct)

module.exports = router
