const router = require('express').Router();
const User = require('../controller/User');

router.post('/register', User.register)
router.post('/login', User.login)

// router.post('/auth', User.auth)

module.exports = router
