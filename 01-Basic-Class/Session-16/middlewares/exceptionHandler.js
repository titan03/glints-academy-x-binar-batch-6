module.exports = function (err, req, res, next) {
    let statusCode = 500
    let message = "Internal Server Error!"

    switch (err.name) {
        case `SequelizeValidationError`:
            let errMsg = []

            err.errors.forEach(i => {
                errMsg.push(i.message)
            })

            statusCode = 400
            message = errMsg
            break;

        case "SequelizeDatabaseError": //constraint allowNull :false
            if (err.parent.code === '23502') {
                statusCode = 400
                message = err.errors[0].message
            }
            break;

        case "SequelizeUniqueConstraintError":
            statusCode = 400
            message = `${err.errors[0].value} already exists`
            break;

        case 'SequelizeForeignKeyConstraintError':
            statusCode = 400
            message = `ForeignKey error!`
            break;

        case `ForbiddenError`:
            statusCode = 403
            message = [err.message]
            break;

        case `NotFoundError`:
            statusCode = 404
            message = [err.message]
            break;

        case `BadRequestError`:
            statusCode = 400
            message = [err.message]
            break;

        case `JsonWebTokenError`:
        case `TokenExpiredError`:
            statusCode = 401
            message = `Invalid Token or Failed to authenticate`
            break;
    }

    statusCode === 500 && console.log(err.stack)

    res.status(statusCode).json({ message })
}