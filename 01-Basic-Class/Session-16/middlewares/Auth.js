const jwt = require('jsonwebtoken');
const { User } = require('../models');
const createError = require('http-errors');

const authentication = (req, res, next) => {
    // try {
    //     const token = req.headers.access_token
    //     console.log(token);

    //     if (!token) {
    //         throw createError(404, "token not found!")
    //     } else {
    //         const decode = jwt.verify(token, process.env.JWT_SECRET)
    //         req.payload = decode //we can use for every file

    // console.log(req.payload, '<============= this is payload');

    //         next()
    //     }
    // } catch (error) {
    //     res.status(400).json({
    //         message: [error.message]
    //     })
    // }


    const token = req.headers.access_token
    // console.log(token, '<========ini token');

    try {
        let decode = jwt.verify(token, process.env.JWT_SECRET)
        // console.log(decode.id, '====================decodeId=========================');
        User.findByPk(decode.id)

            .then((user) => {
                if (user) {
                    req.payload_user = decode
                    // console.log(req.payload_user);
                    next()
                } else {
                    return next(createError(404, 'not found'))
                }
            })
    } catch (error) {
        return next(createError(401, 'Token invalid'))
    }


}

module.exports = authentication
