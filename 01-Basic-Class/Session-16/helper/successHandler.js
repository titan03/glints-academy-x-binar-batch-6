const serveRes = (res, code, result, msg) => {
    /*
    switch (code) {
        case 200:
            return res.status(code).json({
                status: 'Success',
                msg,
                result
            })
        case 201:
            return res.status(code).json({
                status: 'Success',
                msg,
                result
            })
        case 202:
            return res.status(code).json({
                status: 'Success',
                msg,
                token: result
            })
        default:
            console.log('status code not found');
            break;
    }
     */


    if (code === 200) {
        return res.status(code).json({
            status: 'Success',
            msg,
            result
        })
    } else if (code === 201) {
        return res.status(code).json({
            status: 'Successfully created data',
            msg,
            result
        })
    } else if (code === 202) {
        return res.status(code).json({
            status: 'Success',
            msg,
            token: result
        })
    }
}

module.exports = serveRes
