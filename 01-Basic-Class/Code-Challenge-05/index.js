const products = require('./products.json');
const users = require('./user.json');

/*
    Code Challenge #3

    Your goals to create these endpoint
    
    /products
        This will show all products on products.json as JSON Response

    /products/available
        This will show the products which its stock more than 0 as JSON Response

    /users
        This will show the users data inside the users.json,
        But don't show the password!

  */

const http = require('http');
const PORT = 4000
const app = http.createServer((req, res) => {
    switch (req.url) {
        case '/':
            res.write('<h1> Welcom to Mini App </h1>')
            break;
        case '/products':
            res.write(JSON.stringify(products))
            // console.log('ini products');
            break;
        case '/products/available':
            res.write(JSON.stringify(products.filter((product) => {
                return product.stock !== 0
            })))
            // console.log('products/available');
            break;
        case '/users':
            delete (users.password)
            res.write(JSON.stringify(users))
            // console.log('users');
            break;
        case '/error':
            try {
                throw new Error("This is error!");
            }
            catch (err) {
                res.writeHead(500);
                res.write(err.message);
            }
            break;

        default:
            res.writeHead(404);
            res.write("404 Not Found!\n")
            break;
    }
    res.end()
})

app.listen(PORT, () => {
    console.log(`Server running on PORT : ${PORT}`);

})

/* Code Here */