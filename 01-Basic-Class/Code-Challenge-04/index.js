/**
Create a class of Human, which each human can greets another human and marry another human. Here's requirement list:

    - Greet another Human
    - Marry another Human
    - Introduce themself
    - Learn new language
    - If person a tries to marry person b, make sure they've both understand the same language,  otherwise, one of them will learn a new languange that both will understand.
 */

//! To randomly call element inside the array
Object.defineProperty(Array.prototype, 'sample', {
    get: function () {
        return this[
            Math.floor(
                Math.random() * this.length
            )
        ]
    }
})

class Human {
    constructor(props) {
        let { name, address, gender, langs } = props
        this.name = name
        this.address = address
        this.gender = gender
        this.langs = langs
    }

    validate(props) {
        if (typeof props !== 'object' || Array.isArray(props))
            throw new Error("Constructor needs object to work with");

        this.constructor.properties.forEach(i => {
            if (!Object.keys(props).includes(i))
                throw new Error(`${i} is required`);
        });
    }


    introduce(person) {
        console.log(`Hi ${person.name}, my name is ${this.name} and i am from ${this.address}`);
    }

    greeting(person) {
        console.log(`Hello ${person.name}`);
    }

    learn(language) {
        //! Check if the language is already learned by the instance
        //! If it is, then just ignore it!
        if (!this.langs.includes(language))
            this.langs.push(language);
    }

    marry(person) {
        let ready = false;

        //! Check if both bride has the same language
        this.langs.forEach(i => {
            if (person.langs.includes(i)) ready = true;
        })

        //! If not ready then learn new language
        if (!ready) this.learn(person.langs.sample);

        this.spouse = person;
        person.spouse = this;
    }
}

let titan = new Human({ name: 'Titan', address: 'Jaksel', gender: 'Male', langs: ['Bahasa', 'english', 'germany'] })

let woman = new Human({
    name: 'Woman',
    address: 'Jakbar',
    gender: 'Female',
    langs: ['Bahasa', 'Dutch', 'Japanese']
})
// console.log(titan);
titan.introduce(woman)
titan.greeting(woman)
titan.learn('Korean')
titan.marry(woman)
woman.learn('Korean')