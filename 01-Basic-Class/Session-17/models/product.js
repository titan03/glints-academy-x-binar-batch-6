'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sequelize = sequelize.Sequelize
  const Model = Sequelize.Model

  class Product extends Model { }

  Product.init({
    name: {
      type: DataTypes.STRING,
      validate: {
        min: 5,
        notEmpty: {
          msg: 'Please input name'
        }
      }
    },
    price: {
      type: DataTypes.FLOAT,
      validate: {
        isFloat: true,
        notEmpty: {
          msg: 'Please input price'
        }
      }
    },
    stock: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          msg: 'Please input stock'
        },
        isInt: true
      }
    }
  }, { sequelize })
  // const Product = sequelize.define('Product', {

  // }, {});
  Product.associate = function (models) {
    // associations can be defined here
  };
  return Product;
};