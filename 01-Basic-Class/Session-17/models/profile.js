'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    bio: {
      type: DataTypes.STRING,
      validate: {
        msg: `Please input value`
      }
    },
    phone_number: {
      type: DataTypes.STRING,
      validate: {
        msg: `Please input value`
      }
    },
    address: {
      type: DataTypes.TEXT,
      validate: {
        msg: `Please input value`
      }
    },
    photo: {
      type: DataTypes.TEXT,
      validate: {
        msg: `Please input value`
      }
    },
    UserId: DataTypes.INTEGER
  }, {});
  Profile.associate = function (models) {
    // associations can be defined here
  };
  return Profile;
};