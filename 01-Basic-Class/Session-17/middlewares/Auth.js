const jwt = require('jsonwebtoken');
const { User } = require('../models');

const authentication = async (req, res, next) => {
    try {
        let token = req.headers.access_token
        let decode = await jwt.verify(token, process.env.JWT_SECRET)
        req.user = await User.findByPk(decode.id)
        next()
    } catch (err) {
        throw new Error('invalid token')
    }
}

module.exports = authentication