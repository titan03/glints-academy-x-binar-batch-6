const models = require('../models');

module.exports = (modelName) => { //! parameter modelName
    const Model = models[modelName]

    return async (req, res, next) => {
        try {
            let id = req.params.id
            let instance = await Model.findByPK(id)

            if (instance.UserId !== req.user.id) {
                res.status(403).json({
                    status: false,
                    message: 'Forbidden'
                })
            }
        } catch (error) {
            res.status(400).json({
                status: false,
                message: error
            })
        }
    }
}