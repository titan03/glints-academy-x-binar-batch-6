const express = require('express');
const app = express()
const logger = require('morgan')
const Router = require('./router');
require('dotenv').config()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(logger('dev'))

app.get('/', (req, res) => {
    res.send('<h1>Hello World</h1>')
})

app.use(Router)

app.listen(process.env.PORT, () => {
    console.log(`Server started on ${Date(Date.now())}`);
    console.log(`Server is listening on port ${process.env.PORT}`);
});