const { Post, User } = require('../models');


exports.getAll = async (req, res) => {
    try {
        let posts = await Post.findAll({
            include: User,
            order: [
                ['id', 'ASC']
            ]
        })
        res.status(200).json({
            status: 'Success',
            message: `Successfully get all Posts`,
            data: {
                posts
            }
        })
    } catch (error) {
        res.status(500).json({
            status: 'Fail',
            message: `Internal server Error: ${error}`
        })
    }
}

exports.getOne = async (req, res) => {
    try {
        const paramsId = req.params.id
        const post = await Post.findByPk(paramsId)
        res.status(200).json({
            status: 'Succes',
            message: `Seccessfully get one Post`,
            data: {
                post
            }
        })
    } catch (error) {
        res.status(500).json({
            status: 'Fail',
            message: `Internal server Error: ${error}`
        })
    }
}

exports.create = async (req, res) => {
    try {
        const { title, body } = req.body
        let posts = await Post.create({
            title,
            body,
            UserId: req.user.id
        })
        res.status(201).json({
            status: 'Success',
            message: 'Successfully created new user',
            posts
        })
        // console.log(req.user.id, '==========================userId===============');
        // console.log(post);
    } catch (error) {
        res.status(500).json({
            status: 'fail',
            message: `Internal server Error: ${error}`
        })
    }
}

exports.update = async (req, res) => {
    try {
        const postId = req.params.id
        if (Post.UserId === req.user.id) {
            const { title, body } = req.body
            const post = await Post.update({ title, body }, {
                where: { id: postId }
            })
            res.status(201).json({
                status: 'Success',
                message: `Successfuly updated post with Id: ${postId}`,
                result: post
            })
        } else {
            res.status(403).json({
                status: 'Fail',
                message: 'Forbidden'
            })
        }
    } catch (error) {
        res.status(500).json({
            status: 'Fail',
            message: `Internal Sever Error: ${error}`
        })
    }
}

exports.delete = async (req, res) => {
    try {
        const deletedId = req.params.id
        if (Post.UserId === req.user.id) {
            const post = await Post.destroy({ where: { id: deletedId } })
            res.status(200).json({
                status: 'Success',
                message: `Successfully deleted post with Id: ${deletedId}`,
                post
            })
        } else {
            res.status(403).json({
                status: 'Fail',
                message: 'Forbidden'
            })
        }
    } catch (error) {
        res.status(500).json({
            status: 'Fail',
            message: `Internal server Error: ${error}`
        })
    }
}