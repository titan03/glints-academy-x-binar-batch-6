const { Product } = require('../models');
const { Op } = require('sequelize');


exports.getAll = async (req, res) => {
    try {
        const product = await Product.findAll()
        res.status(200).json({
            status: 'succes',
            product
        })
    } catch (error) {
        res.status(500).json({
            message: `Internal server Error: ${error}`
        })
    }
}

exports.getOne = async (req, res) => {
    const productId = req.params.id
    try {
        const product = await Product.findOne({ where: { id: productId } })
        res.status(200).json({
            status: 'Success',
            product
        })
    } catch (error) {
        res.status(500).json({
            message: `Internal server Error: ${error}`
        })
    }
}

exports.getAvailable = async (req, res) => {
    try {
        const product = await Product.findAll({
            where: {
                stock: {
                    [Op.gt]: 2
                }
            }
        })
        res.status(200).json({
            status: 'Success',
            product
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: error
        })
    }
}

exports.create = async (req, res) => {
    try {
        const { name, price, stock } = req.body
        const product = await Product.create({
            name,
            price,
            stock
        })
        res.status(201).json({
            status: 'Success',
            product
        })
    } catch (error) {
        res.status(500).json({
            message: `Internal server Error: ${error}`
        })
    }
}

exports.update = async (req, res) => {
    try {
        const productId = req.params.id
        const { name, price, stock } = req.body
        const product = await Product.update({ name, price, stock }, { where: { id: productId } })
        res.status(201).json({
            status: 'Success',
            product
        })
    } catch (error) {
        res.status(500).json({
            message: `Internal server Error: ${error}`
        })
    }
}

exports.delete = async (req, res) => {
    try {
        const deletedId = req.params.id
        const product = await Product.destroy({ where: { id: deletedId } })
        res.status(200).json({
            status: 'Success',
            product
        })
    } catch (error) {
        res.status(500).json({
            message: `Internal server Error: ${error}`
        })
    }
}