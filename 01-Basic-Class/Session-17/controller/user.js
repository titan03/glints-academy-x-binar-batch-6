const { User, Profile } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.register = async (req, res) => {
    try {
        const { email, password } = req.body
        let user = await User.create({
            email,
            password
        })
        res.status(201).json({
            status: 'Success',
            data: {
                user
            }
        })
    } catch (error) {
        res.status(500).json({
            status: 'fail',
            message: `internal server error ${error}`
        })
    }
}

exports.login = async (req, res) => {
    try {
        const { email, password } = req.body
        let user = await User.findOne({
            where: { email: email.toLowerCase() }
        })
        if (user && bcrypt.compareSync(password, user.password)) {
            let access_token = jwt.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET)
            res.status(202).json({
                status: 'Success',
                message: 'Successfully login',
                access_token
            })
        } else {
            res.status(401).json({
                status: 'fail',
                message: 'input email or password wrong'
            })
        }
    } catch (error) {
        res.status(500).json({
            status: 'fail',
            message: `internal server error ${error}`
        })
    }
}

