const router = require('express').Router();
const User = require('./user');
const Product = require('./product');
const Posts = require('./posts');

router.use('/api/v1/users', User)
router.use('/api/v1/products', Product)
router.use('/api/v1/posts', Posts)

module.exports = router
