const router = require('express').Router();
const Posts = require('../controller/posts');
const Auth = require('../middlewares/Auth');

router.get('/', Auth, Posts.getAll)
router.post('/', Auth, Posts.create)
router.get('/:id', Auth, Posts.getOne)
router.put('/:id', Auth, Posts.update)
router.delete('/:id', Auth, Posts.delete)

module.exports = router
