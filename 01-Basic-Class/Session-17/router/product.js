const router = require('express').Router();
const Product = require('../controller/product');
const Auth = require('../middlewares/Auth');

router.get('/', Product.getAll)
router.get('/:id', Product.getOne)
router.get('/available', Product.getAvailable)

router.post('/', Auth, Product.create)
router.put('/:id', Auth, Product.update)
router.delete('/:id', Auth, Product.delete)

module.exports = router
